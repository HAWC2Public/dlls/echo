---
project: Echo dll
author: DTU Wind and Energy Systems
year: 2024
license: MIT
doc_license: MIT
preprocess: False
project_gitlab: https://gitlab.windenergy.dtu.dk/HAWC2Public/dlls/echo
output_dir: build_docs
display: public
         protected
         private
---

The Echo dll provides a simple concept:
```
output = input
```

This is implemented in three modes of operation, which are meant to work with HAWC2:

- Outputs the input.
- Outputs a state variable from the RAM.
- Outputs a state variable from a text file.

In each case, the input is obtained at each call of the output subroutine.
