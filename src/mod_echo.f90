!***************************************************************************************************
! how to compile
!***************************************************************************************************
! ifort -fpp -shared -fPIC -O3 -Wl,-rpath -Wl,'$ORIGIN'/lib -Wl,-rpath -Wl,'$ORIGIN' ./echo_mod.f90 -o ./Linux/Release/Echo.so
!***************************************************************************************************
! README:
!
!     This module allows to set a state, and retrieve it with HAWC2.
!     There are two modes of operations:
!         1. Everything runs in memory.
!         2. A file is used to read the state from disk. The file is written by another process.
!     In both cases, an external process (for example python) is used to set the state.
!
!***************************************************************************************************

module mod_echo

!***************************************************************************************************

use, intrinsic :: iso_c_binding, only: c_int, c_double, c_char

implicit none

real(kind=c_double), allocatable, dimension(:), save :: state
    !! Array that contains the state of this dll.

integer(kind=c_int), save :: necho
    !! Number of elements in `state` and `input` arrays.

character(len=*, kind=c_char), parameter :: state_file_name = './echo_state.dat'    ! TODO: Get this from htc file.
    !! Path to the file that contains the state.

integer(kind=c_int), save :: fid
    !! Unit to read the state file.

private state, necho, fid


!***************************************************************************************************

contains

!***************************************************************************************************

subroutine init(input, output) bind(C, name='init')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: init
!DEC$ ENDIF

    !! Called by HAWC2 to initialize the dll.

use logging, only: log_info

implicit none

real(kind=c_double), dimension(2), intent(in) :: input
    !! Input array, from HAWC2 to dll. The elements must contain:
    !!
    !!   - 1: Number of echo channels.
    !!   - 2: Operation mode:
    !!        - 0: run in memory. The state is expected to be already updated with `set_state()`.
    !!        - 1: run with disk. The state is read from a text file and returned to HAWC2.

real(kind=c_double), dimension(1), intent(out) :: output
    !! Output array, from dll to HAWC2. Contains the error code in the first element.
    !!
    !! - 0: all fine.
    !! - 1: wrong value for input parameter 2.
    !! - 2: error while opening the state file.

! Error code of input-output operations.
integer(kind=c_int) :: io_err

character(kind=c_char, len=64) :: info_str

! Allocate state array with the required number of elements.
necho = int(input(1))
if (allocated(state)) deallocate(state)
allocate(state(necho))
write(info_str, '(A,I0)') 'Number of echo channels: ', necho
call log_info(trim(info_str))

! Initialize the state to 0.
state = 0.0_c_double

! Check input 2.
select case(int(input(2)))
    case (0)
        ! Run in memory.
        output(1) = 0.0_c_double

    case (1)
        ! Run with disk.
        ! In this case we have to read the state from disk.
        open(newunit=fid, file=state_file_name, form='FORMATTED', status='REPLACE', action='READ', encoding='UTF-8', iostat=io_err)
        if (io_err /= 0) then
            write(*, '(A,I0)') 'Error opening state file. Error code = ', io_err
            output(1) = 2.0_c_double
        else
            ! All fine.
            output(1) = 0.0_c_double
        end if
        
    case default
        write(*, '(A,I0,A)') 'Error: input parameter 2 has been set to ', int(input(2)),&
                             ' but it can only be 0 (memory) or 1 (disk).'
        output(1) = 1.0_c_double
end select

end subroutine init


!***************************************************************************************************


subroutine set_state(input) bind(C, name='set_state')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: set_state
!DEC$ ENDIF

    !! Called by another process, for example python but not HAWC2, to set the new state.
    !! The first `necho` elements of `input` are copied into the `state` array.
    !! Must be called after `init()`.

implicit none

real(kind=c_double), dimension(*), intent(in) :: input
    !! New value of the state. Must have `necho` elements.

! Read the first necho elements of input into state.
state = input(1:necho)

end subroutine set_state


!***************************************************************************************************


pure subroutine get_state_ram(input, output) bind(C, name='get_state_ram')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: get_state_ram
!DEC$ ENDIF

    !! Called by HAWC2 to get the state of the dll. The state is already in memory.
    !! The `state` array is copied into the first `necho` elements of `output`.
    !! Must be called after `init()`.

implicit none

real(kind=c_double), dimension(*), intent(in) :: input
    !! Input array, from HAWC2 to dll. In this case dummy.
real(kind=c_double), dimension(*), intent(out) :: output
    !! Output array, from dll to HAWC2.

output(1:necho) = state

end subroutine get_state_ram


!***************************************************************************************************


subroutine get_state_disk(input, output) bind(C, name='get_state_disk')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: get_state_disk
!DEC$ ENDIF

    !! Called by HAWC2 to get the state of the dll.
    !! The state is loaded from the text file `state_file_name`.
    !! The `state` array is copied into the first `necho` elements of `output`.
    !! Must be called after `init()`.

implicit none

real(kind=c_double), dimension(*), intent(in ) :: input
    !! Input array, from HAWC2 to dll. In this case dummy.
real(kind=c_double), dimension(*), intent(out) :: output
    !! Output array, from dll to HAWC2.

integer(kind=c_int) :: io_err
    !! Error code of input-output operations.

! Read the state file into the state.
rewind(unit=fid, iostat=io_err)
read(unit=fid, fmt=*, iostat=io_err) state

if (io_err /= 0) then
    write(*, '(A,I0)') 'Error while reading the state file. Error code = ', io_err
end if

! Output to HAWC2.
output(1:necho) = state


end subroutine get_state_disk


subroutine echo(input, output) bind(C, name='echo')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: echo
!DEC$ ENDIF

    !! Echo suproutine, that is called by HAWC2 at each time step.
    !! The first `necho` elements of the `input` array are copied to the `output` one.

implicit none

real(kind=c_double), dimension(*), intent(in) :: input
    !! Input array, from HAWC2 to dll.
real(kind=c_double), dimension(*), intent(out) :: output
    !! Output array from dll to HAWC2.

output(1:necho) = input(1:necho)

end subroutine echo


!***************************************************************************************************

end module mod_echo
