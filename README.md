# Echo

[![pipeline status](https://gitlab.windenergy.dtu.dk/HAWC2Public/dlls/echo/badges/main/pipeline.svg)](https://gitlab.windenergy.dtu.dk/HAWC2Public/dlls/echo/-/commits/main)

Provide an echo dll to be used with with HAWC2.

The documentation is available at https://hawc2public.pages.windenergy.dtu.dk/dlls/echo

# Organization of this project

- The source code is contained in `src/src/echo_mod.f90`.
- This library is compiled with the Intel Fortran compiler using cmake and ninja.
- The compiled libraries for Windows 32 bit, Windows 64 bit and Linux is available in the [Package Registry](https://gitlab.windenergy.dtu.dk/HAWC2Public/dlls/echo/-/packages) as a simple zip file. HAWC2 will automatically load the correct file for each platform.
- The test suite and documentation relies on python, and the dependencies are managed via [Poetry](https://python-poetry.org/).
- The documentation is generated with [FORD](https://forddocs.readthedocs.io/en/latest/).
- The [Fortran Package Manager](https://fpm.fortran-lang.org/) will be used for additional testing.
- The main branch is compiled in release mode, while all the others in debug mode. The library compiled in debug mode can be downloaded from the CI artifacts.
